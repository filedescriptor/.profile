# Falk David

Hi, my name is Falk David. I'm a software developer at Blender and I mostly work on Grease Pencil.

## Weekly Reports

- [2021](/filedescriptor/.profile/src/branch/main/reports/2021.md)
- [2023](/filedescriptor/.profile/src/branch/main/reports/2023.md)
- [2024](/filedescriptor/.profile/src/branch/main/reports/2024.md)
- [2025](/filedescriptor/.profile/src/branch/main/reports/2025.md)

## Contact

* [E-Mail](mailto:falk@blender.org)
* [Chat](https://matrix.to/#/@filedescriptor:blender.org)
* [Devtalk](https://devtalk.blender.org/u/filedescriptor/)
